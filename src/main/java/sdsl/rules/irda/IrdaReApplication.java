package sdsl.rules.irda;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.builder.ReleaseId;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.internal.io.ResourceFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@SpringBootApplication
//There are componenets in different packages but all of them has the same base package sdsl.rules
//@SpringBootApplication(scanBasePackages = { "sdsl.rules" }) //moved to @ComponentScan
@ComponentScan(basePackages = "sdsl.rules")
@Configuration
public class IrdaReApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(IrdaReApplication.class, args);
	}

	/*
	 * For deployment: To set the app up as a servlet we extend the main class with
	 * SpringBootServletInitializer and override the configure method using
	 * SpringApplicationBuilder.
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(IrdaReApplication.class);
	}

//	// Return the KieContainer required by all service classes
//	@Bean
//	public KieContainer kieContainer() {
//		return KieServices.Factory.get().getKieClasspathContainer();
//	}
}
