/**
 * 
 */
package sdsl.rules.model;

;

/**
 * @author Sudip Das
 *
 */
public class DischargeSummaryStandard extends ClaimsPack {
//	private int chargeLineItem;
	private double bedCharges;
	private double medicineCharges;
	private double totalCharges;
	private boolean chargeApproved = false;

	public double getBedCharges() {
		return bedCharges;
	}

	public void setBedCharges(double bedCharges) {
		this.bedCharges = bedCharges;
	}

	public double getMedicineCharges() {
		return medicineCharges;
	}

	public void setMedicineCharges(double medicineCharges) {
		this.medicineCharges = medicineCharges;
	}

	public double getTotalCharges() {
		return totalCharges;
	}

	public void setTotalCharges(double totalCharges) {
		this.totalCharges = totalCharges;
	}

	public boolean getChargeApproved() {
		return chargeApproved;
	}

	public void setChargeApproved(boolean chargeApproved) {
		this.chargeApproved = chargeApproved;
	}

}
