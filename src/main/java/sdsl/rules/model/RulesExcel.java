package sdsl.rules.model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;

/**
 * IMPORTANT: DROOLS USES A LOWER VERSION OF POI and hence POI is absent in
 * pom.xml
 * 
 * @author Sudip
 *
 */

public class RulesExcel {

	public String getRulesJson(String ruleFilePath) {
		String returnStr = "";
		try {
			FileInputStream file = new FileInputStream(new File(ruleFilePath));
			HSSFWorkbook workbook = new HSSFWorkbook(file);
			HSSFSheet sheet = workbook.getSheetAt(0);

			/*
			 * Get table names and the row numbers for table headings
			 */
			LinkedHashMap<String, Integer> tableNamesIdxMap = new LinkedHashMap<String, Integer>();

			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {

				Row currRow = rowIterator.next();
				for (Cell cell : currRow) {
					if (cell.getCellType() == Cell.CELL_TYPE_STRING
							&& StringUtils.startsWithIgnoreCase(cell.getStringCellValue(), "RuleTable")) {
						tableNamesIdxMap.put(cell.getStringCellValue(), cell.getRowIndex());
					}
				}
			}

			/*
			 * For each table get columns numbers corresponding to name, condition,
			 * rule-group and action and make independent list for each type. For each type,
			 * use the row number from table heading and column number of each column to get
			 * the cell value. Add to an overall list containing the column details of name,
			 * condition, rule-group and action for each rule table
			 */

			ArrayList<Object> allTablesColDataList = new ArrayList<Object>();

			for (String currTableName : tableNamesIdxMap.keySet()) {
				LinkedHashMap<String, Object> currTableJsonComps = new LinkedHashMap<String, Object>();
				// Overall List per table
				ArrayList<LinkedHashMap<String, Object>> currTableColsList = new ArrayList<LinkedHashMap<String, Object>>();
				// System.err.println("Table Name:"+tableName);
				Row currRow = sheet.getRow(tableNamesIdxMap.get(currTableName) + 1);
				ArrayList<Integer> nameColsList = new ArrayList<Integer>();
				ArrayList<Integer> conditionColsList = new ArrayList<Integer>();
				ArrayList<Integer> rulesGrpColsList = new ArrayList<Integer>();
				ArrayList<Integer> actionsList = new ArrayList<Integer>();

				for (Cell cell : currRow) {
					if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
						if (StringUtils.startsWithIgnoreCase(cell.getStringCellValue(), "NAME")) {
							nameColsList.add(cell.getColumnIndex());
						} else if (StringUtils.startsWithIgnoreCase(cell.getStringCellValue(), "CONDITION")) {
							conditionColsList.add(cell.getColumnIndex());
						} else if (StringUtils.startsWithIgnoreCase(cell.getStringCellValue(), "RULEFLOW-GROUP")) {
							rulesGrpColsList.add(cell.getColumnIndex());
						} else if (StringUtils.startsWithIgnoreCase(cell.getStringCellValue(), "ACTION")) {
							actionsList.add(cell.getColumnIndex());
						}
					}
				}
				// Adding to the current Table List
				currTableColsList.addAll(getColsDataList(sheet, currRow, nameColsList, "NAME"));
				currTableColsList.addAll(getColsDataList(sheet, currRow, conditionColsList, "CONDITION"));
				currTableColsList.addAll(getColsDataList(sheet, currRow, rulesGrpColsList, "RULEFLOW-GROUP"));
				currTableColsList.addAll(getColsDataList(sheet, currRow, actionsList, "ACTION"));

				currTableJsonComps.put("name", currTableName);
				currTableJsonComps.put("columns", currTableColsList);
				allTablesColDataList.add(currTableJsonComps);
			}

//			System.err.println(allTablesColData);

			LinkedHashMap<String, Object> forJSon = new LinkedHashMap<String, Object>();
			forJSon.put("rules", allTablesColDataList);

			Gson gson = new Gson();
			returnStr = gson.toJson(forJSon);
//			System.err.println(returnStr);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return returnStr;
	}

	public ArrayList<LinkedHashMap<String, Object>> getColsDataList(Sheet sheet, Row currRow,
			ArrayList<Integer> colNumbers, String colName) {
		ArrayList<LinkedHashMap<String, Object>> tableColsList = new ArrayList<LinkedHashMap<String, Object>>();

		for (int j = 0; j < colNumbers.size(); j++) {
			LinkedHashMap<String, Object> tempMap = new LinkedHashMap<String, Object>();
			for (int i = currRow.getRowNum() + 1; i < currRow.getRowNum() + 5; i++) {
				Row incrementedRow = sheet.getRow(i);
//			System.err.println("Current Row:" + i);
				for (Cell cell : incrementedRow) {//
//						System.out.println(cell.getColumnIndex()+":"+cell.getStringCellValue());
					if (cell.getColumnIndex() == colNumbers.get(j) && cell.getCellType() == Cell.CELL_TYPE_STRING) {
						tempMap.put("type", colName);
//						System.err.println(cell.getRowIndex() + ":" + cell.getStringCellValue());
						if (currRow.getRowNum() + 3 == cell.getRowIndex()) {
							tempMap.put("name", cell.getStringCellValue());
						}
						if (currRow.getRowNum() + 4 == cell.getRowIndex()) {
							tempMap.put("value", cell.getStringCellValue());
						}

						if (colName.equalsIgnoreCase("NAME") || colName.equalsIgnoreCase("RULEFLOW-GROUP")) {
							tempMap.put("variable", "");
							tempMap.put("code", "");

						} else if (colName.equalsIgnoreCase("CONDITION") || colName.equalsIgnoreCase("ACTION")) {

							if (currRow.getRowNum() + 1 == cell.getRowIndex()) {
								tempMap.put("variable", cell.getStringCellValue());
							}
							if (currRow.getRowNum() + 2 == cell.getRowIndex()) {
								tempMap.put("code", cell.getStringCellValue());
							}
						}
					}
				}
			}

			tableColsList.add(tempMap);
		}
		return tableColsList;
	}

	public boolean setRulesFromJson(String jsonStr, String ruleFilePath) {
		boolean status = true;
		LinkedHashMap<String, ArrayList<LinkedHashMap<String, Object>>> allRuleTablesData = new LinkedHashMap<String, ArrayList<LinkedHashMap<String, Object>>>();
		try {
			Object parsedJSON = new JSONParser().parse(jsonStr);
			JSONObject incomingJsonObj = (JSONObject) parsedJSON;
			JSONArray rulesJsonArray = (JSONArray) incomingJsonObj.get("rules");
//			System.err.println(secondLevelObj);
			Iterator<?> rulesArrayIterator = rulesJsonArray.iterator();

			while (rulesArrayIterator.hasNext()) {
				JSONObject objInRulesArray = (JSONObject) rulesArrayIterator.next();
//				System.out.println(objInRulesArray);
				String currentTableName = (String) objInRulesArray.get("name");
//				System.err.println(tableName);
				JSONArray columnJsonArray = (JSONArray) objInRulesArray.get("columns");
//				System.err.println(columnJsonArray);
				Iterator<?> columnArrayIterator = columnJsonArray.iterator();
				ArrayList<LinkedHashMap<String, Object>> currentTableEntries = new ArrayList<LinkedHashMap<String, Object>>();
				while (columnArrayIterator.hasNext()) {
					LinkedHashMap<String, Object> tableColEntries = new LinkedHashMap<String, Object>();
					JSONObject colJsonObj = (JSONObject) columnArrayIterator.next();
					tableColEntries.put("type", colJsonObj.get("type"));
					tableColEntries.put("variable", colJsonObj.get("variable"));
					tableColEntries.put("code", colJsonObj.get("code"));
					tableColEntries.put("name", colJsonObj.get("name"));
					tableColEntries.put("value", colJsonObj.get("value"));
					currentTableEntries.add(tableColEntries);
//					System.out.println(colEntries);
				}
//				System.out.println(tableEntries);
				allRuleTablesData.put(currentTableName, currentTableEntries);

			}
		} catch (ParseException e) {
			status = false;
			e.printStackTrace();
		}

		if (allRuleTablesData.size() == 0) {
			status = false;
		} else {
			status = writeRulesExcel(allRuleTablesData, ruleFilePath);
		}

//		for (String eachTable : allRuleTablesData.keySet()) {
//			System.err.println(eachTable);
//			System.out.println(allRuleTablesData.get(eachTable));
//		}

		return status;
	}

	public boolean writeRulesExcel(LinkedHashMap<String, ArrayList<LinkedHashMap<String, Object>>> allRuleTablesData,
			String ruleFilePath) {
		boolean success = true;
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet("Sheet1");
		sheet.setColumnWidth(0, 10000);
		sheet.setColumnWidth(1, 10000);
		sheet.setColumnWidth(2, 10000);
		sheet.setColumnWidth(3, 10000);
		sheet.setColumnWidth(4, 10000);
		sheet.setColumnWidth(5, 10000);
		sheet.setColumnWidth(6, 10000);

		CellStyle boldStyleImport = workbook.createCellStyle();
		Font font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);

		boldStyleImport.setFont(font);
		boldStyleImport.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
//		boldStyleImport.setAlignment(HorizontalAlignment.CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);
		boldStyleImport.setFillPattern(CellStyle.SOLID_FOREGROUND);
		boldStyleImport.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());

		CellStyle regularStyleImport = workbook.createCellStyle();
		font = workbook.createFont();
//		font.setBoldweight(false);

		regularStyleImport.setFont(font);
		regularStyleImport.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);
		regularStyleImport.setFillPattern(CellStyle.SOLID_FOREGROUND);
		regularStyleImport.setFillForegroundColor(IndexedColors.LIGHT_YELLOW.getIndex());

		CellStyle tableHeaderStyle = workbook.createCellStyle();
		font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(IndexedColors.WHITE.getIndex());

		tableHeaderStyle.setFont(font);
		tableHeaderStyle.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);
		tableHeaderStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		tableHeaderStyle.setFillForegroundColor(IndexedColors.BLACK.getIndex());

		CellStyle tableSubHeaderStyle = workbook.createCellStyle();
		font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);
		font.setColor(IndexedColors.WHITE.getIndex());

		tableSubHeaderStyle.setFont(font);
		tableSubHeaderStyle.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);
		tableSubHeaderStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		tableSubHeaderStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());

		CellStyle ruleNameCellStyle = workbook.createCellStyle();
		font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);

		ruleNameCellStyle.setFont(font);
		ruleNameCellStyle.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		ruleNameCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);
		ruleNameCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		ruleNameCellStyle.setFillForegroundColor(IndexedColors.LIGHT_GREEN.getIndex());

		CellStyle conditionNameCellStyle = workbook.createCellStyle();
		font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);

		conditionNameCellStyle.setFont(font);
		conditionNameCellStyle.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		conditionNameCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);
		conditionNameCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		conditionNameCellStyle.setFillForegroundColor(IndexedColors.LIGHT_TURQUOISE.getIndex());

		CellStyle ruleGroupCellStyle = workbook.createCellStyle();
		font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);

		ruleGroupCellStyle.setFont(font);
		ruleGroupCellStyle.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		ruleGroupCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);
		ruleGroupCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		ruleGroupCellStyle.setFillForegroundColor(IndexedColors.LAVENDER.getIndex());

		CellStyle ruleActionCellStyle = workbook.createCellStyle();
		font = workbook.createFont();
		font.setBoldweight(Font.BOLDWEIGHT_BOLD);

		ruleActionCellStyle.setFont(font);
		ruleActionCellStyle.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		ruleActionCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);
		ruleActionCellStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
		ruleActionCellStyle.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());

		CellStyle defaultCellStyle = workbook.createCellStyle();
		font = workbook.createFont();
//		font.setBoldweight(false);

		defaultCellStyle.setFont(font);
		defaultCellStyle.setWrapText(true);
		boldStyleImport.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		boldStyleImport.setBorderBottom(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderLeft(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderRight(CellStyle.BORDER_THIN);
		boldStyleImport.setBorderTop(CellStyle.BORDER_THIN);

		int rowCount = 0;
		Row row = sheet.createRow(rowCount);// 0
		Cell cell = row.createCell(0);
		cell.setCellStyle(boldStyleImport);
		cell.setCellValue("RuleSet");
		cell = row.createCell(1);
		cell.setCellStyle(regularStyleImport);
		cell.setCellValue("irda");

		rowCount = rowCount + 1;
		row = sheet.createRow(rowCount);// 1
		cell = row.createCell(0);
		cell.setCellStyle(boldStyleImport);
		cell.setCellValue("Import");
		cell = row.createCell(1);
		cell.setCellStyle(regularStyleImport);
		cell.setCellValue("sdsl.rules.model.ClaimsPack,sdsl.rules.model.DischargeSummaryStandard");

		rowCount = rowCount + 1;
		row = sheet.createRow(rowCount);// 2
		cell = row.createCell(0);
		cell.setCellStyle(boldStyleImport);
		cell.setCellValue("Notes");
		cell = row.createCell(1);
		cell.setCellStyle(regularStyleImport);
		cell.setCellValue("Decision tables for precentage calculation");

		rowCount = rowCount + 2;// 4
		for (String eachTable : allRuleTablesData.keySet()) {
//			System.err.println("CurrentTablName:" + eachTable);
			row = sheet.createRow(rowCount);// Row 4
			cell = row.createCell(0);
			cell.setCellStyle(tableHeaderStyle);
			cell.setCellValue(eachTable);
//			rowCount = rowCount + 1;

			Row row1 = sheet.createRow(rowCount + 1);// Row 5
//			rowCount = rowCount + 1;
			Row row2 = sheet.createRow(rowCount + 2);// Row 6
//			rowCount = rowCount + 1;
			Row row3 = sheet.createRow(rowCount + 3);// Row 7
//			rowCount = rowCount + 1;
			Row row4 = sheet.createRow(rowCount + 4);// Row 8
//			rowCount = rowCount + 1;
			Row row5 = sheet.createRow(rowCount + 5);// Row 9

			ArrayList<LinkedHashMap<String, Object>> tableColsData = allRuleTablesData.get(eachTable);
//			System.out.println("tableColsData:" + tableColsData);

			int nameCellNum = 0;
			int condCellNum = nameCellNum + 1;
			int agendaGrpCellNum = condCellNum + 1;
			int actionCellNum = agendaGrpCellNum + 1;
			for (LinkedHashMap<String, Object> eachColMap : tableColsData) {

//				System.err.println(eachColMap);
				if (eachColMap.get("type").toString().trim().equalsIgnoreCase("NAME")) {
					cell = row1.createCell(nameCellNum);
					cell.setCellStyle(tableSubHeaderStyle);
					cell.setCellValue("NAME");
					cell = row4.createCell(nameCellNum);
					cell.setCellStyle(ruleNameCellStyle);
					cell.setCellValue(eachColMap.get("name").toString());
					cell = row5.createCell(nameCellNum);
					cell.setCellStyle(defaultCellStyle);
					cell.setCellValue(eachColMap.get("value").toString());
				}
				if (eachColMap.get("type").toString().trim().equalsIgnoreCase("CONDITION")) {
//					System.out.println(eachTable+" condCellNum:" + condCellNum);
					cell = row1.createCell(condCellNum);
					cell.setCellStyle(tableSubHeaderStyle);
					cell.setCellValue("CONDITION");
					cell = row2.createCell(condCellNum);
					cell.setCellStyle(defaultCellStyle);
					cell.setCellValue(eachColMap.get("variable").toString());
					cell = row3.createCell(condCellNum);
					cell.setCellStyle(defaultCellStyle);
					cell.setCellValue(eachColMap.get("code").toString());
					cell = row4.createCell(condCellNum);
					cell.setCellStyle(conditionNameCellStyle);
					cell.setCellValue(eachColMap.get("name").toString());
					cell = row5.createCell(condCellNum);
					cell.setCellStyle(defaultCellStyle);
					cell.setCellValue(eachColMap.get("value").toString());
					condCellNum++;

				}

				if (eachColMap.get("type").toString().trim().equalsIgnoreCase("RULEFLOW-GROUP")) {
//					System.out.println(eachTable+" condCellNum:" + condCellNum);
//					System.err.println(eachTable+" agendaGrpCellNum:" + agendaGrpCellNum);
					if (condCellNum > agendaGrpCellNum)
						agendaGrpCellNum = condCellNum;
//					System.err.println(eachTable+" Revised agendaGrpCellNum:" + agendaGrpCellNum);
					cell = row1.createCell(agendaGrpCellNum);
					cell.setCellStyle(tableSubHeaderStyle);
					cell.setCellValue("RULEFLOW-GROUP");
					cell = row4.createCell(agendaGrpCellNum);
					cell.setCellStyle(ruleGroupCellStyle);
					cell.setCellValue(eachColMap.get("name").toString());
					cell = row5.createCell(agendaGrpCellNum);
					cell.setCellStyle(defaultCellStyle);
					cell.setCellValue(eachColMap.get("value").toString());
					agendaGrpCellNum++;
				}

				if (eachColMap.get("type").toString().trim().equalsIgnoreCase("ACTION")) {
					if (agendaGrpCellNum > actionCellNum)
						actionCellNum = agendaGrpCellNum;
					cell = row1.createCell(actionCellNum);
					cell.setCellStyle(tableSubHeaderStyle);
					cell.setCellValue("ACTION");
					cell = row2.createCell(actionCellNum);
					cell.setCellStyle(defaultCellStyle);
					cell.setCellValue(eachColMap.get("variable").toString());
					cell = row3.createCell(actionCellNum);
					cell.setCellStyle(defaultCellStyle);
					cell.setCellValue(eachColMap.get("code").toString());
					cell = row4.createCell(actionCellNum);
					cell.setCellStyle(ruleActionCellStyle);
					cell.setCellValue(eachColMap.get("name").toString());
					cell = row5.createCell(actionCellNum);
					cell.setCellStyle(defaultCellStyle);
					cell.setCellValue(eachColMap.get("value").toString());
					actionCellNum++;
				}
				rowCount = rowCount + 2;
			}

		}

		try {
			File file = new File(ruleFilePath);
			FileOutputStream outputStream = new FileOutputStream(file);
			workbook.write(outputStream);
//			workbook.close();
		} catch (FileNotFoundException e) {
			success = false;
			e.printStackTrace();
		} catch (IOException e) {
			success = false;
			e.printStackTrace();
		}
		return success;
	}

	public static String getJSonToStr(String jsonFile) {
		StringBuffer jsonStrBuff = new StringBuffer();
		try {
			File file = new File(jsonFile);
			InputStream jsonIs = FileUtils.openInputStream(file);
			BufferedReader jsonBr = new BufferedReader(new InputStreamReader(jsonIs));
			String readJsonStr = null;
			while ((readJsonStr = jsonBr.readLine()) != null) {
				jsonStrBuff.append(readJsonStr);
			}
			jsonBr.close();
			jsonIs.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonStrBuff.toString();
	}

	public static void main(String[] args) {
//		RulesExcel rulesExcel = new RulesExcel();
//		String ruleFilePath = SdslREController.getRuleFilePath();
//		rulesExcel.getRulesJson(ruleFilePath);

//		String jsonStr = RulesExcel.getJSonToStr("/Volumes/Sudip/STS/IrdaRE/rulesjson/testrules.json");
//////		System.err.println(jsonStr);
//		rulesExcel.setRulesFromJson(jsonStr, ruleFilePath);
	}

}
