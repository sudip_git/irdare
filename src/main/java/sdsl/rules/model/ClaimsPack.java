package sdsl.rules.model;

import java.util.ArrayList;

public class ClaimsPack {

	private int fileCount = 5;
	private boolean fileCountProper = false;
	private boolean allFilesAvailable=false;
	private ArrayList<String> referenceFileNames = new ArrayList<String>();
	private ArrayList<String> availableFileNames=new ArrayList<String>();


	public int getFileCount() {
		return fileCount;
	}

	public void setFileCount(int count) {
		this.fileCount = count;
	}

	public boolean getFileCountProper() {
		return fileCountProper;
	}

	public void setFileCountProper(boolean fileCountProper) {
		this.fileCountProper = fileCountProper;
	}

	public void checkMergedDocument() {
			if (getFileCount() == 4)
				System.err.println("Checking merged document");
		
	}

	public boolean getAllFilesAvailable() {
		return allFilesAvailable;
	}

	public void setAllFilesAvailable(boolean allFilesAvailable) {
		if(availableFileNames.containsAll(referenceFileNames))
			this.allFilesAvailable = allFilesAvailable;
	}

	public ArrayList<String> getReferenceFileNames() {
		return referenceFileNames;
	}

	public void setReferenceFileNames(ArrayList<String> referenceFileNames) {
		this.referenceFileNames = referenceFileNames;
	}

	public ArrayList<String> getAvailableFileNames() {
		return availableFileNames;
	}

	public void setAvailableFileNames(ArrayList<String> availableFileNames) {
		this.availableFileNames = availableFileNames;
	}
	
}
