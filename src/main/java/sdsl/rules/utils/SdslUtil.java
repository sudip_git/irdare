package sdsl.rules.utils;

public class SdslUtil {

	public static String formatDataTypes(String originalString) {
		String formattedString=originalString.replaceAll("<java.lang.String>", "");
		formattedString = formattedString.replaceAll("java.lang.String", "String");
		formattedString = formattedString.replaceAll("int", "numeric");
		formattedString = formattedString.replaceAll("double", "numeric");
		formattedString = formattedString.replaceAll("float", "numeric");
		formattedString = formattedString.replaceAll("java.util.ArrayList", "List");
		formattedString = formattedString.replaceAll("java.util.List", "List");
		return formattedString;
	}
}
