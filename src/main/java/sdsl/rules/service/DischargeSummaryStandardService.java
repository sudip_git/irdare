package sdsl.rules.service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Service;

import sdsl.rules.controller.SdslREController;
import sdsl.rules.model.DischargeSummaryStandard;
import sdsl.rules.utils.SdslUtil;

@Service
public class DischargeSummaryStandardService implements IModelService {

//	@Autowired
//	private final KieContainer kContainer;

	public static final int BED_CHARGES = 0;
	public static final int MEDICINE_CHARGES = 1;

//	public DischargeSummaryStandardService(KieContainer kContainer) {
//		this.kContainer = kContainer;
//	}

//	public KieContainer getContainer() {
//		KieServices kServices = KieServices.Factory.get();
//		File file = new File("/Volumes/Sudip/STS/IrdaRE/src/main/resources/rules/irda.xls");
//		Resource resource = kServices.getResources().newFileSystemResource(file).setResourceType(ResourceType.DTABLE);
//		KieFileSystem kfs = kServices.newKieFileSystem().write(resource);
//		KieBuilder kb = kServices.newKieBuilder(kfs);
//		kb.buildAll();
//		KieRepository kr = kServices.getRepository();
//		KieContainer kContainer = kServices.newKieContainer(kr.getDefaultReleaseId());
//		return kContainer;
//	}

	public boolean checkLineItemApproval(String kSessionName, int lineItemId, double lineItemCharge,
			double totalCharge) {
//		KieSession kSession = kContainer.newKieSession(kSessionName);
		KieSession kSession = SdslREController.getSession(kSessionName);
		DischargeSummaryStandard claimsDoc = new DischargeSummaryStandard();
		claimsDoc.setTotalCharges(totalCharge);
		try {
			switch (lineItemId) {
			case BED_CHARGES:
				kSession.getAgenda().getAgendaGroup("BED").setFocus();
				claimsDoc.setBedCharges(lineItemCharge);
				break;
			case MEDICINE_CHARGES:
				kSession.getAgenda().getAgendaGroup("MEDICINE").setFocus();
				claimsDoc.setMedicineCharges(lineItemCharge);
				break;
			}

			kSession.insert(claimsDoc);
			kSession.fireAllRules();

		} catch (Throwable t) {
			t.printStackTrace();
		} finally {
			kSession.dispose();
		}
		return claimsDoc.getChargeApproved();
	}

	@Override
	public LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> getClassAttributes() {
		LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> retMap = new LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>();

		ArrayList<LinkedHashMap<String, String>> fieldsList = new ArrayList<LinkedHashMap<String, String>>();
		Field[] declaredFields = getInstanceClass().getDeclaredFields();
		for (Field eachDeclaredField : declaredFields) {
			LinkedHashMap<String, String> eachFieldMap = new LinkedHashMap<String, String>();
			eachFieldMap.put("key", eachDeclaredField.getName());
			Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(eachDeclaredField.getName());
			StringBuffer sb = new StringBuffer();
			while (m.find()) {
			    m.appendReplacement(sb, " "+m.group().toLowerCase());
			}
			m.appendTail(sb);

			eachFieldMap.put("name", sb.toString());
			eachFieldMap.put("type", SdslUtil.formatDataTypes(eachDeclaredField.getGenericType().toString()));
			fieldsList.add(eachFieldMap);

		}
		retMap.put("fields", fieldsList);
		return retMap;
	}

	@Override
	public Class<?> getInstanceClass() {
		return DischargeSummaryStandard.class;
	}
}
