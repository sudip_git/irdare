package sdsl.rules.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public interface IModelService {
	public LinkedHashMap<String, ArrayList<LinkedHashMap<String,String>>>  getClassAttributes();
	
	public Class<?> getInstanceClass();
}
