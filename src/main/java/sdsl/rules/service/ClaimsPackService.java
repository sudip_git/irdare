package sdsl.rules.service;

import java.lang.reflect.Field;
//import java.lang.reflect.Method;
//import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.kie.api.runtime.KieSession;
import org.springframework.stereotype.Service;

import sdsl.rules.controller.SdslREController;
import sdsl.rules.model.ClaimsPack;
import sdsl.rules.utils.SdslUtil;

@Service
public class ClaimsPackService implements IModelService {

//	@Autowired
//	private final KieContainer kContainer;
//
//	public ClaimsPackService(KieContainer kContainer) {
//		this.kContainer = kContainer;
//	}

//	public KieContainer getContainer() {
//		KieServices kServices = KieServices.Factory.get();
//		File file = new File("/Volumes/Sudip/STS/IrdaRE/src/main/resources/rules/irda.xls");
//		Resource resource = kServices.getResources().newFileSystemResource(file).setResourceType(ResourceType.DTABLE);
//		KieFileSystem kfs = kServices.newKieFileSystem().write(resource);
//		KieBuilder kb = kServices.newKieBuilder(kfs);
//		kb.buildAll();
//		KieRepository kr = kServices.getRepository();
//		KieContainer kContainer = kServices.newKieContainer(kr.getDefaultReleaseId());
//		return kContainer;
//	}

	public boolean checkFileCount(String kSessionName, String agendaGrp, int docCount) {

//		KieSession kSession = kContainer.newKieSession(kSessionName);
		KieSession kSession = SdslREController.getSession(kSessionName);
		ClaimsPack doc = new ClaimsPack();
		doc.setFileCount(docCount);

		kSession.getAgenda().getAgendaGroup(agendaGrp).setFocus();
		kSession.insert(doc);
		kSession.fireAllRules();
		kSession.dispose();
		return doc.getFileCountProper();
	}

	public boolean checkFileTypes(String kSessionName, String agendaGrp, ArrayList<String> refDocs,
			ArrayList<String> avlDocs) {
//		KieSession kSession = kContainer.newKieSession(kSessionName);
		KieSession kSession = SdslREController.getSession(kSessionName);
		ClaimsPack doc = new ClaimsPack();
		doc.setReferenceFileNames(refDocs);
		doc.setAvailableFileNames(avlDocs);

		kSession.getAgenda().getAgendaGroup(agendaGrp).setFocus();
		kSession.insert(doc);
		kSession.fireAllRules();
		kSession.dispose();
		return doc.getAllFilesAvailable();
	}

	@Override
	public LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> getClassAttributes() {
		LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> retMap = new LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>();

		ArrayList<LinkedHashMap<String, String>> fieldsList = new ArrayList<LinkedHashMap<String, String>>();
		Field[] declaredFields = getInstanceClass().getDeclaredFields();
		for (Field eachDeclaredField : declaredFields) {
			LinkedHashMap<String, String> eachFieldMap = new LinkedHashMap<String, String>();
			eachFieldMap.put("key", eachDeclaredField.getName());
			Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(eachDeclaredField.getName());
			StringBuffer sb = new StringBuffer();
			while (m.find()) {
			    m.appendReplacement(sb, " "+m.group().toLowerCase());
			}
			m.appendTail(sb);

			eachFieldMap.put("name", sb.toString());
			eachFieldMap.put("type", SdslUtil.formatDataTypes(eachDeclaredField.getGenericType().toString()));
			fieldsList.add(eachFieldMap);

		}
		retMap.put("fields", fieldsList);

//		ArrayList<String> methodsList = new ArrayList<String>();
//		Method[] declaredMethods = getInstanceClass().getDeclaredMethods();
//		for (Method eachDeclaredMethod : declaredMethods) {
//			LinkedHashMap<String,String> eachMethodMap=new LinkedHashMap<String, String>();
//			eachMethodMap.put("Name", eachDeclaredMethod.getName());
//			methodsList.add(eachDeclaredMethod.getName());
//			Parameter[] parameters=eachDeclaredMethod.getParameters();
//			for(Parameter eachParameter:parameters) {
//				System.err.println(eachParameter);
//			}
//		}
//		retMap.put("Methods", methodsList);
		return retMap;
	}

	@Override
	public Class<?> getInstanceClass() {
		return ClaimsPack.class;
	}

}
