package sdsl.rules.service;

import org.springframework.stereotype.Service;

import sdsl.rules.model.RulesExcel;

@Service
public class RulesExcelService {

	private RulesExcel rulesExcel = new RulesExcel();

	public String getRulesJSON(String ruleFilePath) {
		return rulesExcel.getRulesJson(ruleFilePath);
	}

	public String setRulesFromJson(String jsonStr, String ruleFilePath) {
		boolean postStatus = rulesExcel.setRulesFromJson(jsonStr, ruleFilePath);
		if (postStatus)
			return "Successfully update the rules sheet";
		else
			return "Failed to update the rules sheet";
	}
}
