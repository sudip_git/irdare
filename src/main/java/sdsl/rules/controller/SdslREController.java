package sdsl.rules.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieRepository;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import sdsl.rules.model.ClaimsPack;
import sdsl.rules.model.DischargeSummaryStandard;
import sdsl.rules.service.ClaimsPackService;
import sdsl.rules.service.DischargeSummaryStandardService;
import sdsl.rules.service.RulesExcelService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class SdslREController {

//	@Autowired
//	private HttpServletRequest context;
	@Autowired
	private ClaimsPackService claimsPackService;
	@Autowired
	private DischargeSummaryStandardService dischargeSummaryStandardService;
	@Autowired
	private RulesExcelService rulesExcelService;

	private static String ruleFilePath = "./sdslrules/irda.xls";

	public static String getRuleFilePath() {
		return ruleFilePath;
	}

	public static KieSession getSession(String kSessionName) {

//		System.out.println(new File(".").getAbsolutePath());

		KieServices kServices = KieServices.Factory.get();
		File file = new File(ruleFilePath);
		Resource resource = kServices.getResources().newFileSystemResource(file).setResourceType(ResourceType.DTABLE);
		KieFileSystem kfs = kServices.newKieFileSystem().write(resource);
		KieBuilder kb = kServices.newKieBuilder(kfs);
		kb.buildAll();
		KieRepository kr = kServices.getRepository();
		KieContainer kContainer = kServices.newKieContainer(kr.getDefaultReleaseId());
		return kContainer.newKieSession();
	}

	/**
	 * To get the operators JSON from resource folder
	 */
	@RequestMapping(value = "/irda/operators", method = RequestMethod.GET, produces = { "application/json" })
	public String getJSonToStr() {
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		String jsonFile = "operators.json";
		StringBuffer jsonStrBuff = new StringBuffer();
		try {
			InputStream jsonIs = classloader.getResourceAsStream(jsonFile);
			BufferedReader jsonBr = new BufferedReader(new InputStreamReader(jsonIs));
			String readJsonStr = null;
			while ((readJsonStr = jsonBr.readLine()) != null) {
				jsonStrBuff.append(readJsonStr);
			}
			jsonBr.close();
			jsonIs.close();

//			readExternalRuleFile();

		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonStrBuff.toString();
	}

	public void readExternalRuleFile() {
		File directory = new File("./");
		System.err.println(directory.getAbsolutePath());
		File file = new File("./irdarules/irda.xls");
		try {
			FileReader fr = new FileReader(file);
			BufferedReader bfr = new BufferedReader(fr);

			System.out.println(bfr.readLine());

			bfr.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Executing rule for file count
	 * 
	 * @param input
	 * @return
	 */
	@RequestMapping(value = "/irda/filecountcheck", method = RequestMethod.GET, produces = { "application/json" })
	public LinkedHashMap<String, Boolean> checkDocCount(@RequestBody Map<String, Integer> input) {
		LinkedHashMap<String, Boolean> retMap = new LinkedHashMap<String, Boolean>();
		retMap.put("status", claimsPackService.checkFileCount("irda-rule", "DOCCOUNT", input.get("docCount")));
		return retMap;
	}

	/*
	 * Both lists should be present else return false
	 */
	@RequestMapping(value = "/irda/filetypecheck", method = RequestMethod.GET, produces = { "application/json" })
	public LinkedHashMap<String, Boolean> checkDocTypes(@RequestBody Map<String, ArrayList<String>> input) {
		LinkedHashMap<String, Boolean> retMap = new LinkedHashMap<String, Boolean>();
		if (input.get("Reference Docs") == null || input.get("Available Docs") == null) {
			retMap.put("status", false);
		} else {
			retMap.put("status", claimsPackService.checkFileTypes("irda-rule", "DOCTYPE", input.get("Reference Docs"),
					input.get("Available Docs")));
		}
		return retMap;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/irda/lineitemsapproval", method = RequestMethod.GET, produces = { "application/json" })
	public LinkedHashMap<String, LinkedHashMap<String, Boolean>> checkClaimsApproval(
			@RequestBody Map<String, Object> input) {
		LinkedHashMap<String, LinkedHashMap<String, Boolean>> retMap = new LinkedHashMap<String, LinkedHashMap<String, Boolean>>();
		double totalCharge = (Double) input.get("Total Charge");
		ArrayList<Map<String, Object>> lineItemsList = (ArrayList<Map<String, Object>>) input.get("Line Items");

		for (Map<String, Object> eachLineItemMap : lineItemsList) {
			LinkedHashMap<String, Boolean> eachStatusMap = new LinkedHashMap<String, Boolean>();
			String lineItemName = eachLineItemMap.get("name").toString();
			int lineItemId = -1;
			double lineItemCharge = (Double) eachLineItemMap.get("charge");
			if (lineItemName.trim().equalsIgnoreCase("BED")) {
				lineItemId = DischargeSummaryStandardService.BED_CHARGES;
			} else if (lineItemName.trim().equalsIgnoreCase("MEDICINE")) {
				lineItemId = DischargeSummaryStandardService.MEDICINE_CHARGES;
			}
			eachStatusMap.put("Status", dischargeSummaryStandardService.checkLineItemApproval("irda-rule", lineItemId,
					lineItemCharge, totalCharge));
			retMap.put(lineItemName, eachStatusMap);
		}
		return retMap;
	}

	@RequestMapping(value = "/irda/entities", method = RequestMethod.GET, produces = { "application/json" })
	public LinkedHashMap<String, LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>> getEntities() {
		LinkedHashMap<String, LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>> retMap = new LinkedHashMap<String, LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>>();

		retMap.put(ClaimsPack.class.getName(), claimsPackService.getClassAttributes());
		retMap.put(DischargeSummaryStandard.class.getName(), dischargeSummaryStandardService.getClassAttributes());

		return retMap;
	}

	/**
	 * To get the rules JSON from rules folder
	 */
	@RequestMapping(value = "/irda/rules", method = RequestMethod.GET, produces = { "application/json" })
	public String getRulesJSonStr() {
		return rulesExcelService.getRulesJSON(ruleFilePath);
	}

	/**
	 * To update the rules JSON in rules resource folder
	 */
	@RequestMapping(value = "/irda/rules", method = RequestMethod.POST, consumes = "application/json", produces = {
			"text/plain" })
	public String postRulesJSonStr(@RequestBody String payload) {
		return rulesExcelService.setRulesFromJson(payload,ruleFilePath);
	}

}
